# Jojonomic

This website is deployed on [Heroku](https://www.heroku.com/) using [Gitlab](https://gitlab.com/) CI/CD automated deployment. Click [Here](https://jojonomic.herokuapp.com/) to visit this web

## Introduction

This website are using [Nuxt.js](https://nuxtjs.org) for possibility rendered both Server and Client Side. It gives a lot of advantages for quality of this web.
For any reason this website are using some of dependency injection, which is :

- [@nuxtjs/axios](https://axios.nuxtjs.org/#features) - Used for asynchronous API Request
- [bootstrap-vue](https://bootstrap-vue.org/) - CSS Framework
- [vuex](https://vuex.vuejs.org/) - State Management
- [vuex-persist](https://github.com/championswimmer/vuex-persist) - It makes our vuex state persist. I use it to store the `areas` data. `Areas` data have a big size response data provided by API, which is will spending a lot of bandwith if we call it every time we reload the page

> __Attention!__ Since I using a free version of [football-data.org](https://www.football-data.org/) API, it has a limitations of data. It only provide some of competitions data which is competition with ID `2000,2001,2002,2003,2013,2014,2015,2016,2017,2018,2019,2021` 



## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
# Jojonomic Vue

