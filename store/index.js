export const state = () => ({
    counter: 0,
    areas: {},
    teams: {},
})

export const mutations = {
    increment(state){
        state.counter++
    },
    setAreas(state, payload){
        state.areas = payload
    },
    setTeams(state, payload){
        state.teams = payload
    },
}