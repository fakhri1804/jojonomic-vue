export const state = () => ({
    competitions: {
        competitions: []
    },
    selectedArea: {},
    selectedTeam: {},
    teamDetails: {},
    selectedPlayer: {}
})

export const mutations = {
    setCompetitions(state, payload){
        state.competitions = payload
    },
    setSelectedArea(state, payload){
        state.selectedArea = payload
    },
    setSelectedTeam(state, payload){
        state.selectedTeam = payload
    },
    setTeamDetails(state, payload){
        state.teamDetails = payload
    },
    setSelectedPlayer(state, payload){
        state.selectedPlayer = payload
    }
}