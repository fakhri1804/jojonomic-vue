export const state = () => ({
    isLoadingCompetitions: false
})

export const mutations = {
    setLoadingCompetitions(state, payload){
        state.isLoadingCompetitions = payload
    }
}