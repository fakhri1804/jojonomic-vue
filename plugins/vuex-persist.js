import VuexPersistence from 'vuex-persist'

export default ({ store }) => {
  new VuexPersistence({
    modules: ['areas', 'teams']
  }).plugin(store);
}
