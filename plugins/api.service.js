export default function ({ $axios }, inject) {
    // Create a custom axios instance
    const api = $axios.create({
        headers: {
            common: {
              Accept: 'application/json, */*'
            }
        }
    })
    $axios.setHeader('X-Auth-Token', process.env.AUTH_TOKEN)
  
    // Inject to context as $api
    inject('api', api)
}